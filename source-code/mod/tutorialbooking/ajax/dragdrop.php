<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Processes reordering of signup slots in a tutorialbooking activity after an AJAX drag and drop.
 *
 * Please note functions may throw exceptions, please ensure your JS handles them as well as the outcome objects.
 *
 * @package    mod_tutorialbooking
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2014 university of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);

require_once('../../../config.php');

require_sesskey(); // Gotta have the sesskey.

// Process passed parameters.
$tutorialid = required_param('tutorialid', PARAM_INT);
$slotid = required_param('slotid', PARAM_INT);
$targetid = required_param('targetid', PARAM_INT);

$tutorial = $DB->get_record('tutorialbooking', array('id' => $tutorialid), '*', MUST_EXIST);
$cm = get_coursemodule_from_instance('tutorialbooking', $tutorial->id, $tutorial->course, false, MUST_EXIST);
$course = get_course($cm->course);
$context = context_module::instance($cm->id);

require_course_login($course, false, $cm);

$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_pagelayout('incourse');
$PAGE->set_title(get_string('pagetitle', 'mod_tutorialbooking'));
$PAGE->set_url($CFG->wwwroot.'/mod/tutorialbooking/ajax/dragdrop.php');

$PAGE->set_context($context);

// Check the user has the capability to edit the tutorial booking.
require_capability('mod/tutorialbooking:viewadminpage', $context);

// Get the records for the tutorial slots.
list($insql, $params) = $DB->get_in_or_equal(array($slotid, $targetid), SQL_PARAMS_NAMED, 'sessionid');
$slotrecords = $DB->get_records_select('tutorialbooking_sessions', "id $insql", $params);

// Both slots must be in the same tutorial booking.
if ($slotrecords[$slotid]->tutorialid !== $slotrecords[$targetid]->tutorialid) {
    throw new moodle_exception('ajax_sessions_not_in_same_tutorial', 'mod_tutorialbooking', '',
            array('current' => $slotid, 'target' => $targetid));
}

$originalposition = $slotrecords[$slotid]->sequence;
$targetposition = $slotrecords[$targetid]->sequence;

// Start building a response.
$response = new stdClass();
$response->success = mod_tutorialbooking_session::move_sequence($tutorialid, $originalposition, $targetposition);
// Must return before or after based on the direction
// of the move so that the AJAX script can insert the node in the correct position.
$response->where = ($originalposition > $targetposition)? 'before' : 'after';

echo $OUTPUT->header();
echo json_encode($response);
echo $OUTPUT->footer();
