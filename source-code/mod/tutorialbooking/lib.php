<?php
// This file is part of the Tutorial Booking activity.
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants for module tutorialbooking
 *
 * All the core Moodle functions, neeeded to allow the module to work
 * integrated in Moodle is placed here. Tutorialbooking specific functions
 * are in locallib.php.
 *
 * @package    mod_tutorialbooking
 * @copyright  2012 Nottingham University
 * @author     Benjamin Ellis - benjamin.ellis@nottingham.ac.uk
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Returns the information on whether the module supports a feature
 *
 * @see plugin_supports() in lib/moodlelib.php
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function tutorialbooking_supports($feature) {
    switch($feature) {
        case FEATURE_MOD_INTRO:
        case FEATURE_BACKUP_MOODLE2:
        case FEATURE_COMPLETION_TRACKS_VIEWS:
        case FEATURE_COMPLETION_HAS_RULES:
            return true;
        case FEATURE_GROUPS:
        case FEATURE_GROUPINGS:
        case FEATURE_RATE:
        case FEATURE_PLAGIARISM:
            return false;
        default:
            return null;
    }
}

/**
 * Saves a new instance of the tutorialbooking into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param object $tutorialbooking An object from the form in mod_form.php
 * @param mod_tutorialbooking_mod_form $mform
 * @return int The id of the newly inserted tutorialbooking record
 */
function tutorialbooking_add_instance(stdClass $tutorialbooking, mod_tutorialbooking_mod_form $mform = null) {
    global $DB;

    $tutorialbooking->timecreated = time();
    return $DB->insert_record('tutorialbooking', $tutorialbooking);
}

/**
 * Updates an instance of the tutorialbooking in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param object $tutorialbooking An object from the form in mod_form.php
 * @param mod_tutorialbooking_mod_form $mform
 * @return boolean Success/Fail
 */
function tutorialbooking_update_instance(stdClass $tutorialbooking, mod_tutorialbooking_mod_form $mform = null) {
    global $DB;

    $tutorialbooking->timemodified = time();
    $tutorialbooking->id = $tutorialbooking->instance;
    return $DB->update_record('tutorialbooking', $tutorialbooking);
}

/**
 * Removes an instance of the tutorialbooking from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function tutorialbooking_delete_instance($id) {
    global $DB;

    if (!$tutorialbooking = $DB->get_record('tutorialbooking', array('id' => $id))) {
        return false;
    }

    if ($DB->delete_records('tutorialbooking', array('id' => $tutorialbooking->id))) {
        // These will be orphaned records if they fail - so not too serious.
        $DB->delete_records('tutorialbooking_sessions', array('tutorialid' => $tutorialbooking->id));
        // Now the timeslots.
        $DB->delete_records('tutorialbooking_signups',  array('tutorialid' => $tutorialbooking->id));
    } else {
        return false;
    }

    return true;
}

/**
 * Returns all other caps used in the module
 *
 * @example return array('moodle/site:accessallgroups');
 * @return array
 */
function tutorialbooking_get_extra_capabilities() {
    return array('mod/tutorialbooking:submit');
}

/**
 * Obtains the automatic completion state for this tutorialbooking based on any conditions
 * in tutorialbooking settings.
 *
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not. (If no conditions, then return
 *   value depends on comparison type)
 */
function tutorialbooking_get_completion_state($course, $cm, $userid, $type) {
    global $CFG, $DB;

    // Get tutorialbooking details.
    if (!($tutorialbooking = $DB->get_record('tutorialbooking', array('id' => $cm->instance)))) {
        throw new Exception("Can't find tutorial {$cm->instance}");
    }

    $result = $type; // Default return value.

    if (!empty($tutorialbooking->completionsignedup)) { // Users must have signed up to a tutorial slot.
        $signedup = $DB->record_exists('tutorialbooking_signups', array('tutorialid' => $tutorialbooking->id, 'userid' => $userid));

        if ($type == COMPLETION_AND) {
            $result &= $signedup;
        } else {
            $result |= $signedup;
        }
    }

    return $result;
}

/**
 * Extends the settings navigation with the newmodule settings
 *
 * This function is called when the context for the page is a newmodule module. This is not called by AJAX
 * so it is safe to rely on the $PAGE.
 *
 * @param settings_navigation $settingsnav
 * @param navigation_node $newmodulenode
 */
function tutorialbooking_extend_settings_navigation(settings_navigation $settingsnav, navigation_node $tutorialbookingnode=null) {
    global $PAGE;

    if ($tutorialbookingnode === null) {
        $tutorialbookingnode = new navigation_node(get_string('pluginname', 'mod_tutorialbooking'));
    }

    $canedit = $PAGE->user_allowed_editing();
    if ($canedit) {
        $tutorialbookingnode->add(get_string('linktomanagetext', 'tutorialbooking'),
                new moodle_url('/mod/tutorialbooking/tutorialbooking_sessions.php',
                        array('tutorialid' => $PAGE->cm->instance,
                            'courseid' => $PAGE->course->id)));
        $tutorialbookingnode->add(get_string('pagecrumb', 'tutorialbooking'),
                new moodle_url('/mod/tutorialbooking/view.php',
                        array('id' => $PAGE->cm->id,
                            'redirect' => 0)));
    }
}
