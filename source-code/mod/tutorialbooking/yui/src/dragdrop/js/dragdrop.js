// This file is part of the curriculum map course format
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//

/*
 * @package    mod_tutorialbooking
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2014 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

var SLOTCSS = {
    SLOTAREA: 'tutorial_sessions',
    SLOT: 'tutorial_session',
    SLOTNAME: 'sectionname',
    SLOTCONTROLS: 'controls',
    MOVEUPCONTROL: 'moveup',
    MOVEDOWNCONTROL: 'movedown',
    SLOTDRAGABLE: 'slotdraggable',
    DRAGHANDLE: 'editing_move'
};

var DRAGSLOT = function() {
    DRAGSLOT.superclass.constructor.apply(this, arguments);
};

Y.extend(DRAGSLOT, M.core.dragdrop, {
    slotselector : null,

    initializer : function() {
        this.groups = [ SLOTCSS.SLOTDRAGABLE ];
        this.samenodeclass = SLOTCSS.SLOT;
        this.parentnodeclass = SLOTCSS.SLOTAREA;

        this.slotselector = '.'+SLOTCSS.SLOTAREA+' .'+SLOTCSS.SLOT;

        this.setup_slots(this.slotselector);

        var del = new Y.DD.Delegate({
            container: '.'+SLOTCSS.SLOTAREA,
            nodes: '.' + SLOTCSS.SLOTDRAGABLE,
            target: true,
            handles: ['.'+SLOTCSS.SLOTNAME],
            dragConfig: {groups: this.groups}
        });
        del.dd.plug(Y.Plugin.DDProxy, {
            // Don't move the node at the end of the drag
            moveOnEnd: false
        });
        del.dd.plug(Y.Plugin.DDConstrained, {
            // Keep it inside the .course-content
            constrain: '.'+SLOTCSS.SLOTAREA,
            stickY: true
        });
        del.dd.plug(Y.Plugin.DDWinScroll);

        del.dd.plug(Y.Plugin.DDNodeScroll, {
            node: Y.one('.' + this.parentnodeclass)
        });
    },

    /**
     * Changes icons for the
     * @param {string} baseselector
     * @returns {undefined}
     */
    setup_slots : function (baseselector) {
        Y.Node.all(baseselector).each(function(sectionnode) {
            var moveup = sectionnode.one('.'+SLOTCSS.MOVEUPCONTROL);
            var movedown = sectionnode.one('.'+SLOTCSS.MOVEDOWNCONTROL);

            var title = M.util.get_string('moveslot', 'mod_tutorialbooking');

            var slotheader = sectionnode.one('.'+SLOTCSS.SLOTNAME)
                .setAttribute('title', title)
                .setStyle('cursor', 'move');

            // Create an image icon.
            var dragicon = Y.Node.create('<img />')
                .setAttrs({
                    'src' : M.util.image_url('i/move_2d', 'moodle'),
                    'alt' : title
                });

            // Create a span for the image icon.
            var dragelement = Y.Node.create('<span> </span>')
                .setAttribute('tabIndex', 0)
                .setAttribute('role', 'button')
                .setAttribute('data-draggroups', SLOTCSS.SLOTDRAGABLE)
                .addClass(SLOTCSS.DRAGHANDLE)
                .addClass('moodle-core-dragdrop-draghandle');

            // Add the drag icon to the page.
            dragelement.prepend(dragicon);
            slotheader.prepend(dragelement);

            // Remove the exisiting move controls.
            if (moveup) {
                moveup.remove();
            }
            if (movedown) {
                movedown.remove();
            }

            sectionnode.addClass(SLOTCSS.SLOTDRAGABLE);
        });
    },

    /**
     * Copied from the course drag and drop function.
     *
     * @param {Event} e
     * @returns void
     */
    drag_start : function(e) {
        // Get our drag object
        var drag = e.target;
        // Creat a dummy structure of the outer elemnents for clean styles application
        var containernode = Y.Node.create('<div></div>');
        containernode.addClass(SLOTCSS.SLOTAREA);
        var sectionnode = Y.Node.create('<div></div>');
        sectionnode.addClass(this.slotselector);
        sectionnode.setStyle('margin', 0);
        sectionnode.setContent(drag.get('node').get('innerHTML'));
        containernode.appendChild(sectionnode);
        drag.get('dragNode').setContent(containernode);
        drag.get('dragNode').addClass('maincontent');
    },

    drag_dropmiss : function(e) {
        // Missed the target, but we assume the user intended to drop it
        // on the last last ghost node location, e.drag and e.drop should be
        // prepared by global_drag_dropmiss parent so simulate drop_hit(e).
        this.drop_hit(e);
    },

    drop_hit : function(e) {
        var drag = e.drag;
        var drop = e.drop;

        // Prepare an AJAX request.
        var params = {};
        // Prepare request parameters
        params.sesskey = M.cfg.sesskey;
        params.tutorialid = this.get('tutorialid');
        params.slotid = this.get_slot_id(drag.get('node'));
        params.targetid = this.get_slot_id(drop.get('node'));
        Y.log("Moving " + params.slotid + " to " + params.targetid, 'debug', 'mod_tutorialbooking-dragdrop-slot');

        // Do AJAX request
        var uri = this.get('ajaxurl');
        Y.io(uri, {
            method: 'POST',
            data: params,
            on: {
                start : function() {
                    this.lock_drag_handle(drag, SLOTCSS.DRAGHANDLE);
                },
                success: function(tid, response) {
                    var responsearray = Y.JSON.parse(response.responseText);

                    // Check if the script errored.
                    if (responsearray.error) {
                        new M.core.ajaxException(responsearray);
                    }

                    if (responsearray.success) {
                        // The drag was a success.
                        var dragnode = Y.one('#slot-' + params.slotid);
                        var dropnode = Y.one('#slot-' + params.targetid);

                        dragnode.remove();
                        dropnode.insert(dragnode, responsearray.where);
                    } else {
                        Y.log("The node was probably dragged onto itself", 'debug', 'mod_tutorialbooking-dragdrop-slot');
                    }
                    this.unlock_drag_handle(drag, SLOTCSS.DRAGHANDLE);
                },
                failure: function(tid, response) {
                    this.ajax_failure(response);
                    this.unlock_drag_handle(drag, SLOTCSS.DRAGHANDLE);
                }
            },
            context:this
        });
    },

    /**
     * Gets the id number of a dragged slot node.
     *
     * @param {Node} slotnode
     * @returns {String}
     */
    get_slot_id : function(slotnode) {
        // The id should be in the form 'slot-<idnumber>'.
        return slotnode.get('id').substr(5);
    }
}, {
    NAME : 'mod_tutorialbooking-dragdrop-slot',
    ATTRS : {
        tutorialid : {
            'value' : 0
        },
        ajaxurl : {
            'value' : 0
        }
    }
});

M.init_tutorialbooking_dragdrop = function(params) {
    new DRAGSLOT(params);
};
